package main.java.ch.stgerb.bombermanfx.spawns;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

public class BombUpgrade extends Rectangle
{
    private static final int WIDTH = 64;
    private static final int HEIGHT = 64;

    private String path;

    public BombUpgrade(String path, double x, double y)
    {
        Image upgrade = new Image(path);

        setWidth(WIDTH);
        setHeight(HEIGHT);

        setX(x);
        setY(y);

        setFill(new ImagePattern(upgrade));
    }
}