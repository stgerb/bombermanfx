package main.java.ch.stgerb.bombermanfx.views;

import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import main.java.ch.stgerb.bombermanfx.board.Board;
import main.java.ch.stgerb.bombermanfx.board.BoardController;
import main.java.ch.stgerb.bombermanfx.services.GameTimerService;

public class GameView extends BorderPane
{
    private AnchorPane menu;
    private Board board;
    private BoardController controller;
    private GameTimerService timer;
    private boolean pause = false;

    public GameView(AnchorPane menu, Board board, BoardController controller, GameTimerService timer)
    {
        this.menu = menu;
        this.board = board;
        this.controller = controller;
        this.timer = timer;

        setTop(this.menu);
        setCenter(this.board);

        this.controller.startGoombaMovement();

        setOnKeyPressed(new EventHandler<KeyEvent>()
        {
            @Override
            public void handle(KeyEvent event)
            {
                switch (event.getCode())
                {
                    case UP:
                        if (!pause)
                        {
                            controller.movePlayerUp();
                        }
                        break;
                    case DOWN:
                        if (!pause)
                        {
                            controller.movePlayerDown();
                        }
                        break;
                    case LEFT:
                        if (!pause)
                        {
                            controller.movePlayerToTheLeft();
                        }
                        break;
                    case RIGHT:
                        if (!pause)
                        {
                            controller.movePlayerToTheRight();
                        }
                        break;
                    case SPACE:
                        if (!pause)
                        {
                            controller.plantBomb();
                        }
                        break;
                    case ENTER:
                        if (!pause)
                        {
                            controller.stopGoombaMovement();
                            timer.cancel();

                            pause = true;
                        }
                        else
                        {
                            controller.restartGoombaMovement();
                            timer.restart();

                            pause = false;
                        }
                        break;
                }
            }
        });
    }
}