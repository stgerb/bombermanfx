package main.java.ch.stgerb.bombermanfx.board;

public enum Texture
{
    BRICKS(0),
    WEED(1),
    STONE(2),
    WOOD(3);

    private final int value;

    Texture(int value)
    {
        this.value = value;
    }

    public int getValue()
    {
        return value;
    }
}