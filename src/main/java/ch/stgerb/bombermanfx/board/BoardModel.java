package main.java.ch.stgerb.bombermanfx.board;

import javafx.animation.ScaleTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.util.Duration;
import main.java.ch.stgerb.bombermanfx.callbacks.BombExplosionCallback;
import main.java.ch.stgerb.bombermanfx.callbacks.DeathCallback;
import main.java.ch.stgerb.bombermanfx.callbacks.ItemCallback;

public class BoardModel
{
    private static final String PLAYER = "player";
    private static final String GOOMBA = "goomba";

    public void movePlayerUp(Player player, Block neighborBlock, Block previousBlock, DeathCallback callback, ItemCallback item)
    {
        if (!isPlayerColliding(neighborBlock))
        {
            playerUp(player, neighborBlock, previousBlock);
        }
        else
        {
            if (neighborBlock.hasExplosion() || neighborBlock.hasGoomba())
            {
                playerUp(player, neighborBlock, previousBlock);
                callback.onPlayerDeath();
            }
            else if (neighborBlock.hasExplosionUpgrade())
            {
                playerUp(player, neighborBlock, previousBlock);
                item.onExplosionUpgradePickUp(neighborBlock);
            }
            else if (neighborBlock.hasBombUpgrade())
            {
                playerUp(player, neighborBlock, previousBlock);
                item.onBombUpgradePickUp(neighborBlock);
            }
            else if (neighborBlock.hasDoor())
            {
                playerUp(player, neighborBlock, previousBlock);
                item.onDoorCollision(neighborBlock);
            }
        }
    }

    public void movePlayerDown(Player player, Block neighborBlock, Block previousBlock, DeathCallback callback, ItemCallback item)
    {
        if (!isPlayerColliding(neighborBlock))
        {
            playerDown(player, neighborBlock, previousBlock);
        }
        else
        {
            if (neighborBlock.hasExplosion() || neighborBlock.hasGoomba())
            {
                playerDown(player, neighborBlock, previousBlock);
                callback.onPlayerDeath();
            }
            else if (neighborBlock.hasExplosionUpgrade())
            {
                playerDown(player, neighborBlock, previousBlock);
                item.onExplosionUpgradePickUp(neighborBlock);
            }
            else if (neighborBlock.hasBombUpgrade())
            {
                playerDown(player, neighborBlock, previousBlock);
                item.onBombUpgradePickUp(neighborBlock);
            }
            else if (neighborBlock.hasDoor())
            {
                playerDown(player, neighborBlock, previousBlock);
                item.onDoorCollision(neighborBlock);
            }
        }
    }

    public void movePlayerToTheLeft(Player player, Block neighborBlock, Block previousBlock, DeathCallback callback, ItemCallback item)
    {
        if (!isPlayerColliding(neighborBlock))
        {
            playerLeft(player, neighborBlock, previousBlock);
        }
        else
        {
            if (neighborBlock.hasExplosion() || neighborBlock.hasGoomba())
            {
                playerLeft(player, neighborBlock, previousBlock);
                callback.onPlayerDeath();
            }
            else if (neighborBlock.hasExplosionUpgrade())
            {
                playerLeft(player, neighborBlock, previousBlock);
                item.onExplosionUpgradePickUp(neighborBlock);
            }
            else if (neighborBlock.hasBombUpgrade())
            {
                playerLeft(player, neighborBlock, previousBlock);
                item.onBombUpgradePickUp(neighborBlock);
            }
            else if (neighborBlock.hasDoor())
            {
                playerLeft(player, neighborBlock, previousBlock);
                item.onDoorCollision(neighborBlock);
            }
        }
    }

    public void movePlayerToTheRight(Player player, Block neighborBlock, Block previousBlock, DeathCallback callback, ItemCallback item)
    {
        if (!isPlayerColliding(neighborBlock))
        {
            playerRight(player, neighborBlock, previousBlock);
        }
        else
        {
            if (neighborBlock.hasExplosion() || neighborBlock.hasGoomba())
            {
                playerRight(player, neighborBlock, previousBlock);
                callback.onPlayerDeath();
            }
            else if (neighborBlock.hasExplosionUpgrade())
            {
                playerRight(player, neighborBlock, previousBlock);
                item.onExplosionUpgradePickUp(neighborBlock);
            }
            else if (neighborBlock.hasBombUpgrade())
            {
                playerRight(player, neighborBlock, previousBlock);
                item.onBombUpgradePickUp(neighborBlock);
            }
            else if (neighborBlock.hasDoor())
            {
                playerRight(player, neighborBlock, previousBlock);
                item.onDoorCollision(neighborBlock);
            }
        }
    }

    private void playerUp(Player player, Block neighborBlock, Block previousBlock)
    {
        neighborBlock.setPlayer(true);
        previousBlock.setPlayer(false);

        player.setY(player.getY() - player.getHeight());
        player.decRow();
    }

    private void playerDown(Player player, Block neighborBlock, Block previousBlock)
    {
        neighborBlock.setPlayer(true);
        previousBlock.setPlayer(false);

        player.setY(player.getY() + player.getHeight());
        player.incRow();
    }

    private void playerLeft(Player player, Block neighborBlock, Block previousBlock)
    {
        neighborBlock.setPlayer(true);
        previousBlock.setPlayer(false);

        player.setX(player.getX() - player.getWidth());
        player.decColumn();
    }

    private void playerRight(Player player, Block neighborBlock, Block previousBlock)
    {
        neighborBlock.setPlayer(true);
        previousBlock.setPlayer(false);

        player.setX(player.getX() + player.getWidth());
        player.incColumn();
    }

    private boolean isPlayerColliding(Block block)
    {
        if (block.isBricks() || block.isStone() || block.isWood() || block.hasBomb() || block.hasExplosion() || block.hasGoomba() || block.hasBombUpgrade() || block.hasExplosionUpgrade() || block.hasDoor())
        {
            return true;
        }

        return false;
    }

    public boolean isGoombaColliding(Block block)
    {
        if (block.isWood() || block.isBricks() || block.isStone() || block.hasBomb() || block.hasGoomba() || block.hasPlayer() || block.hasExplosion())
        {
            return true;
        }

        return false;
    }

    private void goombaUp(Goomba goomba, Block neighborBlock, Block previousBlock)
    {
        neighborBlock.setGoomba(true, goomba);
        previousBlock.setGoomba(false, null);

        goomba.setY(goomba.getY() - goomba.getHeight());
        goomba.decRow();
    }

    private void goombaDown(Goomba goomba, Block neighborBlock, Block previousBlock)
    {
        neighborBlock.setGoomba(true, goomba);
        previousBlock.setGoomba(false, null);

        goomba.setY(goomba.getY() + goomba.getHeight());
        goomba.incRow();
    }

    private void goombaLeft(Goomba goomba, Block neighborBlock, Block previousBlock)
    {
        neighborBlock.setGoomba(true, goomba);
        previousBlock.setGoomba(false, null);

        goomba.setX(goomba.getX() - goomba.getWidth());
        goomba.decColumn();
    }

    private void goombaRight(Goomba goomba, Block neighborBlock, Block previousBlock)
    {
        neighborBlock.setGoomba(true, goomba);
        previousBlock.setGoomba(false, null);

        goomba.setX(goomba.getX() + goomba.getWidth());
        goomba.incColumn();
    }

    public String moveGoombaUp(Goomba goomba, Block neighborBlock, Block previousBlock)
    {
        if (!isGoombaColliding(neighborBlock))
        {
            goombaUp(goomba, neighborBlock, previousBlock);
        }
        else
        {
            if (neighborBlock.hasPlayer())
            {
                goombaUp(goomba, neighborBlock, previousBlock);

                return PLAYER;
            }
            else if (neighborBlock.hasExplosion())
            {
                goombaUp(goomba, neighborBlock, previousBlock);

                return GOOMBA;
            }
        }
        return null;
    }

    public String moveGoombaDown(Goomba goomba, Block neighborBlock, Block previousBlock)
    {
        if (!isGoombaColliding(neighborBlock))
        {
            goombaDown(goomba, neighborBlock, previousBlock);
        }
        else
        {
            if (neighborBlock.hasPlayer())
            {
                goombaDown(goomba, neighborBlock, previousBlock);

                return PLAYER;
            }
            else if (neighborBlock.hasExplosion())
            {
                goombaDown(goomba, neighborBlock, previousBlock);

                return GOOMBA;
            }
        }
        return null;
    }

    public String moveGoombaToTheLeft(Goomba goomba, Block neighborBlock, Block previousBlock)
    {
        if (!isGoombaColliding(neighborBlock))
        {
            goombaLeft(goomba, neighborBlock, previousBlock);
        }
        else
        {
            if (neighborBlock.hasPlayer())
            {
                goombaLeft(goomba, neighborBlock, previousBlock);

                return PLAYER;
            }
            else if (neighborBlock.hasExplosion())
            {
                goombaLeft(goomba, neighborBlock, previousBlock);

                return GOOMBA;
            }
        }
        return null;
    }

    public String moveGoombaToTheRight(Goomba goomba, Block neighborBlock, Block previousBlock)
    {
        if (!isGoombaColliding(neighborBlock))
        {
            goombaRight(goomba, neighborBlock, previousBlock);
        }
        else
        {
            if (neighborBlock.hasPlayer())
            {
                goombaRight(goomba, neighborBlock, previousBlock);

                return PLAYER;
            }
            else if (neighborBlock.hasExplosion())
            {
                goombaRight(goomba, neighborBlock, previousBlock);

                return GOOMBA;
            }
        }
        return null;
    }

    public Bomb plantBomb(Player player, Block block)
    {
        if (player.getPlantedBombsCount() < player.getMaxPlaceableBombsCount() && !block.hasBomb() && !block.hasDoor() && !block.hasBombUpgrade() && !block.hasExplosionUpgrade())
        {
            block.setBomb(true);
            player.incPlantedBombsCount();

            return new Bomb("/main/resources/images/bomb.png", player.getX(), player.getY(), player.getRow(), player.getColumn());
        }

        return null;
    }

    public void setBombAnimation(Bomb bomb, Block block, Player player, BombExplosionCallback callback)
    {
        ScaleTransition st = new ScaleTransition(Duration.millis(150), bomb);
        st.setByX(0.10f);
        st.setByY(0.10f);
        st.setAutoReverse(true);
        st.setCycleCount(15);
        st.play();
        st.setOnFinished(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent event)
            {
                callback.onExplode(bomb);
                block.setBomb(false);
                player.decPlantedBombsCount();
            }
        });
    }

    public void handleExplosion(Explosion explosion, Block block, BombExplosionCallback callback)
    {
        block.setExplosion(true);

        ScaleTransition st = new ScaleTransition(Duration.millis(150), explosion);
        st.setByX(0.10f);
        st.setByY(0.10f);
        st.setAutoReverse(true);
        st.setCycleCount(7);
        st.play();
        st.setOnFinished(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent event)
            {
                callback.afterExplosion(explosion, block);

                block.changeTexture("/main/resources/images/gras.png");
                block.setWeed(true);
                block.setExplosion(false);
            }
        });
    }

    public Explosion createBottomExplosion(Bomb bomb, int i)
    {
        return new Explosion("/main/resources/images/explosion.png", bomb.getX(), bomb.getY() + i * bomb.getHeight());
    }

    public Explosion createTopExplosion(Bomb bomb, int i)
    {
        return new Explosion("/main/resources/images/explosion.png", bomb.getX(), bomb.getY() - i * bomb.getHeight());
    }

    public Explosion createLeftExplosion(Bomb bomb, int i)
    {
        return new Explosion("/main/resources/images/explosion.png", bomb.getX() - i * bomb.getWidth(), bomb.getY());
    }

    public Explosion createRightExplosion(Bomb bomb, int i)
    {
        return new Explosion("/main/resources/images/explosion.png", bomb.getX() + i * bomb.getWidth(), bomb.getY());
    }
}