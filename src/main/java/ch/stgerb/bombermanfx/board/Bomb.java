package main.java.ch.stgerb.bombermanfx.board;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;


public class Bomb extends Rectangle
{
    private static final int WIDTH = 64;
    private static final int HEIGHT = 64;

    private String path;
    private int row;
    private int column;

    public Bomb(String path, double x, double y, int row, int column)
    {
        Image bomb = new Image(path);

        this.row = row;
        this.column = column;

        setWidth(WIDTH);
        setHeight(HEIGHT);

        setX(x);
        setY(y);

        setFill(new ImagePattern(bomb));
    }

    public int getRow()
    {
        return row;
    }

    public int getColumn()
    {
        return column;
    }
}