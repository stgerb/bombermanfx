package main.java.ch.stgerb.bombermanfx.board;

import javafx.application.Platform;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Duration;
import main.java.ch.stgerb.bombermanfx.callbacks.BombExplosionCallback;
import main.java.ch.stgerb.bombermanfx.callbacks.DeathCallback;
import main.java.ch.stgerb.bombermanfx.callbacks.ItemCallback;
import main.java.ch.stgerb.bombermanfx.controllers.GameMenuController;
import main.java.ch.stgerb.bombermanfx.services.GameTimerService;
import main.java.ch.stgerb.bombermanfx.services.GoombaMovementService;
import main.java.ch.stgerb.bombermanfx.spawns.BombUpgrade;
import main.java.ch.stgerb.bombermanfx.spawns.Door;
import main.java.ch.stgerb.bombermanfx.spawns.ExplosionUpgrade;
import main.java.ch.stgerb.bombermanfx.util.StageUtil;
import main.java.ch.stgerb.bombermanfx.views.GameView;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class BoardController implements DeathCallback, BombExplosionCallback, ItemCallback
{
    private static final int GOOMBA_MOVEMENT_SPEED = 250;
    private static final int MAX_EXPLOSION_UPGRADE_SPAWN = 4;
    private static final int MAX_BOMB_UPGRADE_SPWAN = 3;
    private static final int GOOMBA_COUNT = 3;
    private static final double ITEM_SPWAN_PROBABILITY = 0.25;

    private Board view;
    private BoardModel model;

    private Player player;

    private GameTimerService timer;

    private GoombaMovementService goomba1Movement = null;
    private GoombaMovementService goomba2Movement = null;
    private GoombaMovementService goomba3Movement = null;

    private Stage s;

    private int explosionUpgradeSpwanCount = 0;
    private int bombUpgradeSpwanCount = 0;
    private int goombaCount = GOOMBA_COUNT;

    private StageUtil stage = new StageUtil();

    private int currentLevel = 0;

    public BoardController(Board view, BoardModel model, Stage s, GameTimerService timer)
    {
        this.view = view;
        this.model = model;
        this.s = s;
        this.timer = timer;

        player = this.view.getPlayer();
        Block startBlock = this.view.getBlock(player.getRow(), player.getColumn());
        startBlock.setPlayer(true);

        s.setOnHidden(new EventHandler<WindowEvent>()
        {
            @Override
            public void handle(WindowEvent event)
            {
                saveGameState();
            }
        });
    }

    public void saveGameState()
    {
        Platform.runLater(new Runnable()
        {
            @Override
            public void run()
            {
                // Save map
                int[][] level = view.getLevel();
                StringBuilder builder = new StringBuilder();
                for (int x = 0; x < level.length; x++)
                {
                    for (int y = 0; y < level[x].length; y++)
                    {
                        Block block = view.getBlock(x, y);
                        if (block.isWood())
                        {
                            builder.append("" + Texture.WOOD.getValue());
                        }
                        else if (block.isStone())
                        {
                            builder.append("" + Texture.STONE.getValue());
                        }
                        else if (block.isBricks())
                        {
                            builder.append("" + Texture.BRICKS.getValue());
                        }
                        else if (block.isWeed())
                        {
                            builder.append("" + Texture.WEED.getValue());
                        }

                        if (y < 16)
                            builder.append(",");
                    }
                    if (x < 12)
                    {
                        builder.append("\n");
                    }
                }

                try
                {
                    BufferedWriter writer = new BufferedWriter(new FileWriter("files/map.txt"));
                    writer.write(builder.toString());
                    writer.close();
                }
                catch (IOException e)
                {
                    System.out.println(e.getMessage());
                }

                // Save player data
                StringBuilder builder2 = new StringBuilder();
                builder2.append(view.getPlayer().getX() + ",");
                builder2.append(view.getPlayer().getY() + ",");
                builder2.append(view.getPlayer().getRow() + ",");
                builder2.append(view.getPlayer().getColumn() + ",");
                builder2.append(view.getPlayer().getExplosionRadius() + ",");
                builder2.append(view.getPlayer().getMaxPlaceableBombsCount());

                try
                {
                    BufferedWriter writer2 = new BufferedWriter(new FileWriter("files/player.txt"));
                    writer2.write(builder2.toString());
                    writer2.close();
                }
                catch (IOException e)
                {
                    System.out.println(e.getMessage());
                }

                // Save level
                StringBuilder builder3 = new StringBuilder();
                builder3.append(currentLevel);

                try
                {
                    BufferedWriter writer3 = new BufferedWriter(new FileWriter("files/level.txt"));
                    writer3.write(builder3.toString());
                    writer3.close();
                }
                catch (IOException e)
                {
                    System.out.println(e.getMessage());
                }

                // Save spawn counts
                StringBuilder builder4 = new StringBuilder();
                builder4.append(explosionUpgradeSpwanCount + ",");
                builder4.append(bombUpgradeSpwanCount);

                try
                {
                    BufferedWriter writer4 = new BufferedWriter(new FileWriter("files/spawned_items.txt"));
                    writer4.write(builder4.toString());
                    writer4.close();
                }
                catch (IOException e)
                {
                    System.out.println(e.getMessage());
                }

                // Save game time
                StringBuilder builder5 = new StringBuilder();
                builder5.append(timer.getSeconds() + ",");
                builder5.append(timer.getMinutes());

                try
                {
                    BufferedWriter writer5 = new BufferedWriter(new FileWriter("files/time.txt"));
                    writer5.write(builder5.toString());
                    writer5.close();
                }
                catch (IOException e)
                {
                    System.out.println(e.getMessage());
                }

                // Save Goomba data
                StringBuilder builder6 = new StringBuilder();
                builder6.append(view.getGoomba1().isAlive() + ",");
                builder6.append(view.getGoomba1().getX() + ",");
                builder6.append(view.getGoomba1().getY() + ",");
                builder6.append(view.getGoomba1().getRow() + ",");
                builder6.append(view.getGoomba1().getColumn() + ",");
                builder6.append(view.getGoomba1().getIdentifier() + ",");
                builder6.append(view.getGoomba2().isAlive() + ",");
                builder6.append(view.getGoomba2().getX() + ",");
                builder6.append(view.getGoomba2().getY() + ",");
                builder6.append(view.getGoomba2().getRow() + ",");
                builder6.append(view.getGoomba2().getColumn() + ",");
                builder6.append(view.getGoomba2().getIdentifier() + ",");
                builder6.append(view.getGoomba3().isAlive() + ",");
                builder6.append(view.getGoomba3().getX() + ",");
                builder6.append(view.getGoomba3().getY() + ",");
                builder6.append(view.getGoomba3().getRow() + ",");
                builder6.append(view.getGoomba3().getColumn() + ",");
                builder6.append(view.getGoomba3().getIdentifier() + ",");
                builder6.append(goombaCount);

                try
                {
                    BufferedWriter writer6 = new BufferedWriter(new FileWriter("files/goomba.txt"));
                    writer6.write(builder6.toString());
                    writer6.close();
                }
                catch (IOException e)
                {
                    System.out.println(e.getMessage());
                }
            }
        });
    }

    public void movePlayerUp()
    {
        Block neighborBlock = view.getBlock(player.getRow() - 1, player.getColumn());
        Block previousBlock = view.getBlock(player.getRow(), player.getColumn());

        model.movePlayerUp(player, neighborBlock, previousBlock, this, this);
    }

    public void movePlayerDown()
    {
        Block neighborBlock = view.getBlock(player.getRow() + 1, player.getColumn());
        Block previousBlock = view.getBlock(player.getRow(), player.getColumn());

        model.movePlayerDown(player, neighborBlock, previousBlock, this, this);
    }

    public void movePlayerToTheLeft()
    {
        Block neighborBlock = view.getBlock(player.getRow(), player.getColumn() - 1);
        Block previousBlock = view.getBlock(player.getRow(), player.getColumn());

        model.movePlayerToTheLeft(player, neighborBlock, previousBlock, this, this);
    }

    public void movePlayerToTheRight()
    {
        Block neighborBlock = view.getBlock(player.getRow(), player.getColumn() + 1);
        Block previousBlock = view.getBlock(player.getRow(), player.getColumn());

        model.movePlayerToTheRight(player, neighborBlock, previousBlock, this, this);
    }

    public void plantBomb()
    {
        Block block = view.getBlock(player.getRow(), player.getColumn());

        Bomb bomb = model.plantBomb(player, block);
        if (bomb != null)
        {
            view.getChildren().add(bomb);
            model.setBombAnimation(bomb, block, player, this);
        }
    }

    public void startGoombaMovement()
    {
        Goomba goomba1 = view.getGoomba1();
        Goomba goomba2 = view.getGoomba2();
        Goomba goomba3 = view.getGoomba3();

        if (goomba1 != null)
        {
            goomba1Movement = new GoombaMovementService(model, view, goomba1);
            goomba1Movement.setPeriod(Duration.millis(GOOMBA_MOVEMENT_SPEED));
            goomba1Movement.setOnSucceeded(new EventHandler<WorkerStateEvent>()
            {
                @Override
                public void handle(WorkerStateEvent event)
                {
                    Goomba goomba = view.getGoomba1();
                    goomba1Movement.updateGoomba(goomba);

                    if (event.getSource().getValue() != null)
                    {
                        handleDeath(goomba, event);
                    }
                }
            });
            goomba1Movement.start();
        }

        if (goomba2 != null)
        {
            goomba2Movement = new GoombaMovementService(model, view, goomba2);
            goomba2Movement.setPeriod(Duration.millis(GOOMBA_MOVEMENT_SPEED));
            goomba2Movement.setOnSucceeded(new EventHandler<WorkerStateEvent>()
            {
                @Override
                public void handle(WorkerStateEvent event)
                {
                    Goomba goomba = view.getGoomba2();
                    goomba2Movement.updateGoomba(goomba);

                    if (event.getSource().getValue() != null)
                    {
                        handleDeath(goomba, event);
                    }
                }
            });
            goomba2Movement.start();
        }

        if (goomba3 != null)
        {
            goomba3Movement = new GoombaMovementService(model, view, goomba3);
            goomba3Movement.setPeriod(Duration.millis(GOOMBA_MOVEMENT_SPEED));
            goomba3Movement.setOnSucceeded(new EventHandler<WorkerStateEvent>()
            {
                @Override
                public void handle(WorkerStateEvent event)
                {
                    Goomba goomba = view.getGoomba3();
                    goomba3Movement.updateGoomba(goomba);

                    if (event.getSource().getValue() != null)
                    {
                        handleDeath(goomba, event);
                    }
                }
            });
            goomba3Movement.start();
        }
    }

    private void handleDeath(Goomba goomba, WorkerStateEvent event)
    {
        String whoDied = event.getSource().getValue().toString();
        switch (whoDied)
        {
            case "player":
                onPlayerDeath();
                break;
            case "goomba":
                Block block = view.getBlock(goomba.getRow(), goomba.getColumn());
                onGoombaDeath(goomba, block);
                break;
        }
    }

    public void stopGoombaMovement()
    {
        if (goomba1Movement != null)
        {
            goomba1Movement.cancel();
        }

        if (goomba2Movement != null)
        {
            goomba2Movement.cancel();
        }

        if (goomba3Movement != null)
        {
            goomba3Movement.cancel();
        }
    }

    public void restartGoombaMovement()
    {
        if (goomba1Movement != null)
        {
            goomba1Movement.restart();
        }

        if (goomba2Movement != null)
        {
            goomba2Movement.restart();
        }

        if (goomba3Movement != null)
        {
            goomba3Movement.restart();
        }
    }

    @Override
    public void onExplode(Bomb bomb)
    {
        view.getChildren().remove(bomb);

        for (int i = 1; i < player.getExplosionRadius(); i++)
        {
            Block blockRight = view.getBlock(bomb.getRow(), bomb.getColumn() + i);

            if (blockRight.isStone() || blockRight.isBricks())
            {
                break;
            }
            else
            {
                Explosion explosion = model.createRightExplosion(bomb, i);
                view.getChildren().add(explosion);

                if (blockRight.hasPlayer())
                {
                    onPlayerDeath();
                }
                else if (blockRight.hasGoomba())
                {
                    Goomba goomba = blockRight.getGoombaObj();
                    onGoombaDeath(goomba, blockRight);

                    model.handleExplosion(explosion, blockRight, this);
                }
                else if (blockRight.hasExplosionUpgrade())
                {
                    onExplosionUpgradeDeath(blockRight);

                    model.handleExplosion(explosion, blockRight, this);
                }
                else if (blockRight.hasBombUpgrade())
                {
                    onBombUpgradeDeath(blockRight);

                    model.handleExplosion(explosion, blockRight, this);
                }
                else
                {
                    model.handleExplosion(explosion, blockRight, this);
                }
            }
        }

        for (int i = 0; i < player.getExplosionRadius(); i++)
        {
            Block blockBottom = view.getBlock(bomb.getRow() + i, bomb.getColumn());

            if (blockBottom.isStone() || blockBottom.isBricks())
            {
                break;
            }
            else
            {
                Explosion explosion = model.createBottomExplosion(bomb, i);
                view.getChildren().add(explosion);

                if (blockBottom.hasPlayer())
                {
                    onPlayerDeath();
                }
                else if (blockBottom.hasExplosionUpgrade())
                {
                    onExplosionUpgradeDeath(blockBottom);

                    model.handleExplosion(explosion, blockBottom, this);
                }
                else if (blockBottom.hasBombUpgrade())
                {
                    onBombUpgradeDeath(blockBottom);

                    model.handleExplosion(explosion, blockBottom, this);

                }
                else if (blockBottom.hasGoomba())
                {
                    Goomba goomba = blockBottom.getGoombaObj();
                    onGoombaDeath(goomba, blockBottom);

                    model.handleExplosion(explosion, blockBottom, this);
                }
                else
                {
                    model.handleExplosion(explosion, blockBottom, this);
                }
            }
        }

        for (int i = 0; i < player.getExplosionRadius(); i++)
        {
            Block blockTop = view.getBlock(bomb.getRow() - i, bomb.getColumn());

            if (blockTop.isStone() || blockTop.isBricks())
            {
                break;
            }
            else
            {
                Explosion explosion = model.createTopExplosion(bomb, i);
                view.getChildren().add(explosion);

                if (blockTop.hasPlayer())
                {
                    onPlayerDeath();
                }
                else if (blockTop.hasExplosionUpgrade())
                {
                    onExplosionUpgradeDeath(blockTop);

                    model.handleExplosion(explosion, blockTop, this);
                }
                else if (blockTop.hasBombUpgrade())
                {
                    onBombUpgradeDeath(blockTop);

                    model.handleExplosion(explosion, blockTop, this);
                }
                else if (blockTop.hasGoomba())
                {
                    Goomba goomba = blockTop.getGoombaObj();
                    onGoombaDeath(goomba, blockTop);

                    model.handleExplosion(explosion, blockTop, this);
                }
                else
                {
                    model.handleExplosion(explosion, blockTop, this);
                }
            }
        }

        for (int i = 0; i < player.getExplosionRadius(); i++)
        {
            Block blockLeft = view.getBlock(bomb.getRow(), bomb.getColumn() - i);

            if (blockLeft.isStone() || blockLeft.isBricks())
            {
                break;
            }
            else
            {
                Explosion explosion = model.createLeftExplosion(bomb, i);
                view.getChildren().add(explosion);

                if (blockLeft.hasPlayer())
                {
                    onPlayerDeath();
                }
                else if (blockLeft.hasExplosionUpgrade())
                {
                    onExplosionUpgradeDeath(blockLeft);

                    model.handleExplosion(explosion, blockLeft, this);
                }
                else if (blockLeft.hasBombUpgrade())
                {
                    onBombUpgradeDeath(blockLeft);

                    model.handleExplosion(explosion, blockLeft, this);
                }
                else if (blockLeft.hasGoomba())
                {
                    Goomba goomba = blockLeft.getGoombaObj();
                    onGoombaDeath(goomba, blockLeft);

                    model.handleExplosion(explosion, blockLeft, this);
                }
                else
                {
                    model.handleExplosion(explosion, blockLeft, this);
                }
            }
        }
    }

    @Override
    public void afterExplosion(Explosion explosion, Block block)
    {
        view.getChildren().remove(explosion);

        if (block.isWood())
        {
            if (new Random().nextDouble() < ITEM_SPWAN_PROBABILITY)
            {
                int randNum = new Random().nextInt(2);

                switch (randNum)
                {
                    case 0:
                        bombUpgradeSpwanCount++;

                        if (bombUpgradeSpwanCount < MAX_BOMB_UPGRADE_SPWAN)
                        {
                            BombUpgrade bombUpgrade = new BombUpgrade("/main/resources/images/bomb_upgrade.png", block.getX(), block.getY());
                            view.getChildren().add(bombUpgrade);
                            block.setBombUpgradeObj(true, bombUpgrade);

                            break;
                        }
                    case 1:
                        explosionUpgradeSpwanCount++;

                        if (explosionUpgradeSpwanCount < MAX_EXPLOSION_UPGRADE_SPAWN)
                        {
                            ExplosionUpgrade explosionUpgrade = new ExplosionUpgrade("/main/resources/images/explosion_upgrade.png", block.getX(), block.getY());
                            view.getChildren().add(explosionUpgrade);
                            block.setExplosionUpgrade(true, explosionUpgrade);

                            break;
                        }
                }
            }
        }
    }

    @Override
    public void onPlayerDeath()
    {
        timer.cancel();

        stopGoombaMovement();

        try
        {
            stage.changeFxml(s, "/main/resources/fxml/game_over.fxml");
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void onGoombaDeath(Goomba goomba, Block block)
    {
        goomba.setAlive(false);

        goombaCount--;

        switch (goomba.getIdentifier())
        {
            case 1:
                goomba1Movement.cancel();
                break;
            case 2:
                goomba2Movement.cancel();
                break;
            case 3:
                goomba3Movement.cancel();
                break;
        }

        view.getChildren().remove(goomba);
        block.setGoomba(false, null);

        if (goombaCount == 0)
        {
            Door door = new Door("/main/resources/images/door.png", block.getX(), block.getY());
            view.getChildren().add(door);
            block.setDoor(true);
        }
    }

    @Override
    public void onExplosionUpgradeDeath(Block block)
    {
        view.getChildren().remove(block.getExplosionUpgradeObj());
        block.setExplosionUpgrade(false, null);
    }

    @Override
    public void onBombUpgradeDeath(Block block)
    {
        view.getChildren().remove(block.getBombUpgradeObj());
        block.setBombUpgradeObj(false, null);
    }

    @Override
    public void onExplosionUpgradePickUp(Block block)
    {
        view.getChildren().remove(block.getExplosionUpgradeObj());

        block.setExplosionUpgrade(false, null);
        player.incExplosionRadius();
    }

    @Override
    public void onBombUpgradePickUp(Block block)
    {
        view.getChildren().remove(block.getBombUpgradeObj());

        block.setBombUpgradeObj(false, null);
        player.incMaxPlaceableBombsCount();
    }

    @Override
    public void onDoorCollision(Block block)
    {
        timer.cancel();

        stopGoombaMovement();

        currentLevel++;

        if (currentLevel == 2)
        {
            try
            {
                stage.changeFxml(s, "/main/resources/fxml/main_menu.fxml");
            }
            catch (IOException e)
            {
                System.out.println(e.getMessage());
            }
        }
        else
        {
            int[][] level = new int[][]{
                    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                    {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
                    {0, 1, 2, 1, 2, 3, 2, 3, 2, 3, 2, 3, 2, 1, 2, 1, 0},
                    {0, 1, 1, 1, 1, 3, 3, 3, 3, 3, 3, 3, 3, 1, 1, 1, 0},
                    {0, 1, 2, 1, 1, 1, 2, 3, 2, 3, 2, 3, 2, 3, 2, 1, 0},
                    {0, 3, 3, 3, 1, 1, 1, 3, 3, 3, 3, 3, 3, 3, 1, 1, 0},
                    {0, 3, 3, 3, 3, 1, 1, 1, 3, 3, 3, 3, 3, 1, 1, 1, 0},
                    {0, 3, 3, 3, 3, 3, 1, 1, 1, 3, 3, 3, 1, 1, 1, 1, 0},
                    {0, 1, 2, 3, 2, 1, 2, 1, 1, 1, 2, 1, 1, 1, 2, 1, 0},
                    {0, 1, 1, 1, 1, 1, 3, 3, 1, 1, 1, 1, 1, 1, 3, 1, 0},
                    {0, 1, 2, 3, 2, 3, 2, 3, 2, 1, 1, 1, 2, 1, 2, 1, 0},
                    {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
                    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
            };

            Player player = new Player("/main/resources/images/player.png", 64, 64, 1, 1);
            Goomba goomba1 = new Goomba("/main/resources/images/goomba.png", 960, 64, 1, 15, 1, true);
            Goomba goomba2 = new Goomba("/main/resources/images/goomba.png", 64, 704, 11, 1, 2, true);
            Goomba goomba3 = new Goomba("/main/resources/images/goomba.png", 960, 704, 11, 15, 3, true);
            Board board = new Board(level, player, goomba1, goomba2, goomba3);

            GameTimerService timer = new GameTimerService(0, 10);
            BoardController controller = new BoardController(board, new BoardModel(), s, timer);
            controller.setCurrentLevel(currentLevel);

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/main/resources/fxml/game_menu.fxml"));
            loader.setControllerFactory(param -> new GameMenuController(controller, timer));
            AnchorPane menu = null;
            try
            {
                menu = loader.load();
            }
            catch (IOException e)
            {
                System.out.println(e.getMessage());
            }

            GameView game = new GameView(menu, board, controller, timer);

            stage.setBorderPane(s, game);

            game.requestFocus();
        }
    }

    public void setCurrentLevel(int level)
    {
        this.currentLevel = level;
    }

    public void setMaxExplosionUpgradeSpawn(int count)
    {
        explosionUpgradeSpwanCount = count;
    }

    public void setMaxBombUpgradeSpwan(int count)
    {
        bombUpgradeSpwanCount = count;
    }

    public void setGoombaCount(int count)
    {
        goombaCount = count;
    }
}