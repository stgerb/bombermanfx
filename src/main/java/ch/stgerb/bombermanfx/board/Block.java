package main.java.ch.stgerb.bombermanfx.board;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import main.java.ch.stgerb.bombermanfx.spawns.BombUpgrade;
import main.java.ch.stgerb.bombermanfx.spawns.ExplosionUpgrade;

public class Block extends Rectangle
{
    private static final int WIDTH = 64;
    private static final int HEIGHT = 64;
    private boolean weed;
    private boolean bricks;
    private boolean wood;
    private boolean stone;
    private boolean bomb = false;
    private Goomba goombaObj;
    private boolean goomba = false;
    private boolean explosion = false;
    private boolean player = false;
    private boolean explosionUpgrade;
    private boolean bombUpgrade;
    private boolean door;
    private ExplosionUpgrade explosionUpgradeObj;
    private BombUpgrade bombUpgradeObj;

    public Block(String path, boolean weed, boolean bricks, boolean wood, boolean stone, double x, double y)
    {
        this.weed = weed;
        this.bricks = bricks;
        this.wood = wood;
        this.stone = stone;
        Image texture = new Image(path);
        setWidth(WIDTH);
        setHeight(HEIGHT);
        setX(x * WIDTH);
        setY(y * HEIGHT);
        setFill(new ImagePattern(texture));
    }

    public boolean isWeed()
    {
        return weed;
    }

    public boolean isBricks()
    {
        return bricks;
    }

    public boolean isWood()
    {
        return wood;
    }

    public boolean isStone()
    {
        return stone;
    }

    public boolean hasExplosionUpgrade()
    {
        if (explosionUpgrade)
        {
            return true;
        }
        return false;
    }

    public boolean hasDoor()
    {
        if (door)
        {
            return true;
        }
        return false;
    }

    public boolean hasBombUpgrade()
    {
        if (bombUpgrade)
        {
            return true;
        }
        return false;
    }

    public void setDoor(boolean bool)
    {
        door = bool;
    }

    public void setExplosionUpgrade(boolean bool, ExplosionUpgrade explosionUpgradeObj)
    {
        explosionUpgrade = bool;
        this.explosionUpgradeObj = explosionUpgradeObj;
    }

    public void setBombUpgradeObj(boolean bool, BombUpgrade bombUpgradeObj)
    {
        bombUpgrade = bool;
        this.bombUpgradeObj = bombUpgradeObj;
    }

    public ExplosionUpgrade getExplosionUpgradeObj()
    {
        return explosionUpgradeObj;
    }

    public BombUpgrade getBombUpgradeObj()
    {
        return bombUpgradeObj;
    }

    public boolean hasGoomba()
    {
        if (goomba)
        {
            return true;
        }
        return false;
    }

    public boolean hasPlayer()
    {
        if (player)
        {
            return true;
        }
        return false;
    }

    public boolean hasExplosion()
    {
        if (explosion)
        {
            return true;
        }
        return false;
    }

    public void setPlayer(boolean bool)
    {
        player = bool;
    }

    public void setExplosion(boolean bool)
    {
        explosion = bool;
    }

    public void setGoomba(boolean bool, Goomba goombaObj)
    {
        goomba = bool;
        this.goombaObj = goombaObj;
    }

    public Goomba getGoombaObj()
    {
        return goombaObj;
    }

    public void setBomb(boolean bool)
    {
        bomb = bool;
    }

    public boolean hasBomb()
    {
        if (bomb)
        {
            return true;
        }
        return false;
    }

    public void setWeed(boolean bool)
    {
        weed = bool;
    }

    public void clearTexture()
    {
        weed = false;
        stone = false;
        bricks = false;
        wood = false;
    }

    public void changeTexture(String path)
    {
        clearTexture();

        Image texture = new Image(path);
        setFill(new ImagePattern(texture));

    }
}