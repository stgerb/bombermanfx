package main.java.ch.stgerb.bombermanfx.services;

import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import main.java.ch.stgerb.bombermanfx.board.Block;
import main.java.ch.stgerb.bombermanfx.board.Board;
import main.java.ch.stgerb.bombermanfx.board.BoardModel;
import main.java.ch.stgerb.bombermanfx.board.Goomba;

import java.util.concurrent.ThreadLocalRandom;

public class GoombaMovementService extends ScheduledService<String>
{
    private BoardModel model;
    private Board view;
    private Goomba goomba;
    private String whoDied = null;

    public GoombaMovementService(BoardModel model, Board view, Goomba goomba)
    {
        this.model = model;
        this.view = view;
        this.goomba = goomba;
    }

    @Override
    protected Task<String> createTask()
    {
        return new Task<String>()
        {
            @Override
            protected String call() throws Exception
            {
                Block neighborBlock;
                Block currentBlock;

                int direction = ThreadLocalRandom.current().nextInt(1, 4 + 1);
                switch (direction)
                {
                    case 1:
                        neighborBlock = view.getBlock(goomba.getRow() - 1, goomba.getColumn());
                        currentBlock = view.getBlock(goomba.getRow(), goomba.getColumn());

                        whoDied = model.moveGoombaUp(goomba, neighborBlock, currentBlock);
                        break;
                    case 2:
                        neighborBlock = view.getBlock(goomba.getRow(), goomba.getColumn() + 1);
                        currentBlock = view.getBlock(goomba.getRow(), goomba.getColumn());

                        whoDied = model.moveGoombaToTheRight(goomba, neighborBlock, currentBlock);
                        break;
                    case 3:
                        neighborBlock = view.getBlock(goomba.getRow() + 1, goomba.getColumn());
                        currentBlock = view.getBlock(goomba.getRow(), goomba.getColumn());

                        whoDied = model.moveGoombaDown(goomba, neighborBlock, currentBlock);
                        break;
                    case 4:
                        neighborBlock = view.getBlock(goomba.getRow(), goomba.getColumn() - 1);
                        currentBlock = view.getBlock(goomba.getRow(), goomba.getColumn());

                        whoDied = model.moveGoombaToTheLeft(goomba, neighborBlock, currentBlock);
                        break;
                }

                return whoDied;
            }
        };
    }

    public void updateGoomba(Goomba goomba)
    {
        this.goomba = goomba;
    }
}