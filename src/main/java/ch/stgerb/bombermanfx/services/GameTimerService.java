package main.java.ch.stgerb.bombermanfx.services;

import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;

public class GameTimerService extends ScheduledService<String>
{
    private int seconds;
    private int minutes;

    public GameTimerService(int seconds, int minutes)
    {
        this.seconds = seconds;
        this.minutes = minutes;
    }

    public GameTimerService()
    {

    }

    @Override
    protected Task<String> createTask()
    {
        return new Task<String>()
        {
            @Override
            protected String call() throws Exception
            {
                String text = null;

                if (minutes == 0 && seconds == 0)
                {
                    cancel();
                }
                else if (seconds == 0)
                {
                    seconds = 59;
                    minutes--;

                    if (minutes >= 0 && minutes < 10)
                    {
                        text = String.format("0%s:%s", Integer.toString(minutes), Integer.toString(seconds));
                    }
                    else
                    {
                        text = String.format("%s:%s", Integer.toString(minutes), Integer.toString(seconds));
                    }
                }
                else
                {
                    seconds--;

                    if (seconds >= 10)
                    {
                        if (minutes >= 0 && minutes < 10)
                        {
                            text = String.format("0%s:%s", Integer.toString(minutes), Integer.toString(seconds));
                        }
                        else
                        {
                            text = String.format("%s:%s", Integer.toString(minutes), Integer.toString(seconds));
                        }
                    }
                    else if (seconds < 10)
                    {
                        if (minutes >= 0 && minutes < 10)
                        {
                            text = String.format("0%s:0%s", Integer.toString(minutes), Integer.toString(seconds));
                        }
                        else
                        {
                            text = String.format("%s:0%s", Integer.toString(minutes), Integer.toString(seconds));
                        }
                    }
                }

                return text;
            }
        };
    }

    public int getSeconds()
    {
        return seconds;
    }

    public int getMinutes()
    {
        return minutes;
    }

    public void setSeconds(int seconds)
    {
        this.seconds = seconds;
    }

    public void setMinutes(int minutes)
    {
        this.minutes = minutes;
    }
}