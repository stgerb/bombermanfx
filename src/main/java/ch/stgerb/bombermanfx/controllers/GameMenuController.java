package main.java.ch.stgerb.bombermanfx.controllers;

import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.stage.Stage;
import javafx.util.Duration;
import main.java.ch.stgerb.bombermanfx.board.BoardController;
import main.java.ch.stgerb.bombermanfx.services.GameTimerService;
import main.java.ch.stgerb.bombermanfx.util.StageUtil;

import java.io.IOException;

public class GameMenuController
{
    private StageUtil stage = new StageUtil();
    private BoardController controller;
    private GameTimerService timer;

    @FXML
    private MenuBar menuBar;

    @FXML
    private Label lblTimer;

    @FXML
    public void initialize()
        {
            lblTimer.textProperty().bind(timer.lastValueProperty());
    }

    public GameMenuController(BoardController controller, GameTimerService timer)
    {
        this.controller = controller;
        this.timer = timer;

        this.timer.setPeriod(Duration.seconds(1));

        this.timer.setOnSucceeded(new EventHandler<WorkerStateEvent>()
        {
            @Override
            public void handle(WorkerStateEvent event)
            {
                if (event.getSource().getValue().equals("00:00"))
                {
                    controller.stopGoombaMovement();

                    Stage s = (Stage) menuBar.getScene().getWindow();
                    try
                    {
                        stage.changeFxml(s, "/main/resources/fxml/game_over.fxml");
                    }
                    catch (IOException e)
                    {
                        System.out.println(e.getMessage());
                    }
                    finally
                    {
                        System.out.println("Time's up! Game Over!");
                    }
                }
            }
        });

        this.timer.start();
    }

    @FXML
    public void onClickHowToPlay(ActionEvent event) throws IOException
    {
        stage.newStage("/main/resources/fxml/controls2.fxml", "BombermanFX");
    }

    @FXML
    public void onClickBackToMainMenu(ActionEvent event) throws IOException
    {
        timer.cancel();
        controller.stopGoombaMovement();

        Stage s = (Stage) menuBar.getScene().getWindow();
        stage.changeFxml(s, "/main/resources/fxml/main_menu.fxml");

        controller.saveGameState();
    }
}