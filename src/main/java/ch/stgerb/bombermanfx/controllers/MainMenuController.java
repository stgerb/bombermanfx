package main.java.ch.stgerb.bombermanfx.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import main.java.ch.stgerb.bombermanfx.board.*;
import main.java.ch.stgerb.bombermanfx.services.GameTimerService;
import main.java.ch.stgerb.bombermanfx.util.StageUtil;
import main.java.ch.stgerb.bombermanfx.views.GameView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MainMenuController
{
    private StageUtil stage = new StageUtil();

    @FXML
    public void onClickExit()
    {
        System.exit(0);
    }

    @FXML
    public void onClickNewGame(ActionEvent event) throws IOException
    {
        int[][] level = new int[][]{
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 1, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 0},
                {0, 1, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 1, 0},
                {0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 0},
                {0, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 1, 0},
                {0, 3, 3, 3, 3, 3, 3, 3, 3, 1, 1, 1, 1, 1, 1, 1, 0},
                {0, 3, 2, 3, 2, 3, 2, 3, 2, 1, 2, 1, 2, 3, 2, 3, 0},
                {0, 3, 3, 3, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 3, 0},
                {0, 3, 2, 3, 2, 1, 2, 3, 2, 1, 2, 3, 2, 1, 2, 3, 0},
                {0, 1, 1, 1, 1, 1, 3, 3, 3, 1, 1, 1, 1, 1, 3, 3, 0},
                {0, 1, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 1, 2, 3, 0},
                {0, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
        };

        Player player = new Player("/main/resources/images/player.png", 64, 64, 1, 1);
        Goomba goomba1 = new Goomba("/main/resources/images/goomba.png", 960, 64, 1, 15, 1, true);
        Goomba goomba2 = new Goomba("/main/resources/images/goomba.png", 64, 704, 11, 1, 2, true);
        Goomba goomba3 = new Goomba("/main/resources/images/goomba.png", 960, 704, 11, 15, 3, true);
        Board board = new Board(level, player, goomba1, goomba2, goomba3);

        Stage s = (Stage) ((Node) event.getSource()).getScene().getWindow();

        GameTimerService timer = new GameTimerService(0, 10);
        BoardController controller = new BoardController(board, new BoardModel(), s, timer);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/main/resources/fxml/game_menu.fxml"));
        loader.setControllerFactory(param -> new GameMenuController(controller, timer));
        AnchorPane menu = loader.load();

        GameView game = new GameView(menu, board, controller, timer);

        stage.setBorderPane(s, game);

        game.requestFocus();

        File file1 = new File("files/goomba.txt");
        if (file1.exists())
        {
            file1.delete();
        }

        File file2 = new File("files/level.txt");
        if (file2.exists())
        {
            file2.delete();
        }

        File file3 = new File("files/map.txt");
        if (file3.exists())
        {
            file3.delete();
        }

        File file4 = new File("files/player.txt");
        if (file4.exists())
        {
            file4.delete();
        }

        File file5 = new File("files/spawned_items.txt");
        if (file5.exists())
        {
            file5.delete();
        }

        File file6 = new File("files/time.txt");
        if (file6.exists())
        {
            file6.delete();
        }
    }

    @FXML
    public void onClickResumeGame(ActionEvent event) throws IOException
    {
        // Load level data
        File file = new File("files/map.txt");
        if (file.exists())
        {
            int[][] level = new int[13][17];
            try
            {
                BufferedReader reader = new BufferedReader(new FileReader(file));
                String line = "";
                int row = 0;
                while ((line = reader.readLine()) != null)
                {
                    String[] cols = line.split(",");
                    int col = 0;
                    for (String c : cols)
                    {
                        level[row][col] = Integer.parseInt(c);
                        col++;
                    }
                    row++;
                }
                reader.close();
            }
            catch (IOException e)
            {
                System.out.println(e.getMessage());
            }

            // load player data
            File file2 = new File("files/player.txt");
            double playerX = 0;
            double playerY = 0;
            int playerRow = 0;
            int playerColumn = 0;
            int explosionRadius = 0;
            int maxPlaceableBombsCount = 0;

            try
            {
                BufferedReader reader2 = new BufferedReader(new FileReader(file2));
                String line = "";
                while ((line = reader2.readLine()) != null)
                {
                    String[] data = line.split(",");

                    for (int i = 0; i < data.length; i++)
                    {
                        switch (i)
                        {
                            case 0:
                                playerX = Double.parseDouble(data[i]);
                                break;
                            case 1:
                                playerY = Double.parseDouble(data[i]);
                                break;
                            case 2:
                                playerRow = Integer.parseInt(data[i]);
                                break;
                            case 3:
                                playerColumn = Integer.parseInt(data[i]);
                                break;
                            case 4:
                                explosionRadius = Integer.parseInt(data[i]);
                                break;
                            case 5:
                                maxPlaceableBombsCount = Integer.parseInt(data[i]);
                                break;
                        }
                    }
                }
                reader2.close();
            }
            catch (IOException e)
            {
                System.out.println(e.getMessage());
            }

            // load level data
            File file3 = new File("files/level.txt");
            int currentLevel = 0;
            try
            {
                BufferedReader reader3 = new BufferedReader(new FileReader(file3));
                String line = "";
                while ((line = reader3.readLine()) != null)
                {
                    currentLevel = Integer.parseInt(line);
                }
                reader3.close();
            }
            catch (IOException e)
            {
                System.out.println(e.getMessage());
            }

            File file4 = new File("files/spawned_items.txt");
            int bombUpgradeSpawnCount = 0;
            int explosionUpgradeSpawnCount = 0;

            try
            {
                BufferedReader reader4 = new BufferedReader(new FileReader(file4));
                String line = "";
                while ((line = reader4.readLine()) != null)
                {
                    String[] data = line.split(",");

                    for (int i = 0; i < data.length; i++)
                    {
                        switch (i)
                        {
                            case 0:
                                explosionUpgradeSpawnCount = Integer.parseInt(data[i]);
                                break;
                            case 1:
                                bombUpgradeSpawnCount = Integer.parseInt(data[i]);
                                break;
                        }
                    }
                }
                reader4.close();
            }
            catch (IOException e)
            {
                System.out.println(e.getMessage());
            }

            // load game time
            File file5 = new File("files/time.txt");
            int seconds = 0;
            int minutes = 0;

            try
            {
                BufferedReader reader5 = new BufferedReader(new FileReader(file5));
                String line = "";
                while ((line = reader5.readLine()) != null)
                {
                    String[] data = line.split(",");

                    for (int i = 0; i < data.length; i++)
                    {
                        switch (i)
                        {
                            case 0:
                                seconds = Integer.parseInt(data[i]);
                                break;
                            case 1:
                                minutes = Integer.parseInt(data[i]);
                                break;
                        }
                    }
                }
                reader5.close();
            }
            catch (IOException e)
            {
                System.out.println(e.getMessage());
            }

            // load goomba data
            File file6 = new File("files/goomba.txt");
            boolean goomba1Alive = true;
            double goomba1X = 0;
            double goomba1Y = 0;
            int goomba1Row = 0;
            int goomba1Column = 0;
            int goomba1Identifier = 0;
            boolean goomba2Alive = true;
            double goomba2X = 0;
            double goomba2Y = 0;
            int goomba2Row = 0;
            int goomba2Column = 0;
            int goomba2Identifier = 0;
            boolean goomba3Alive = true;
            double goomba3X = 0;
            double goomba3Y = 0;
            int goomba3Row = 0;
            int goomba3Column = 0;
            int goomba3Identifier = 0;
            int goombaCount = 0;

            try
            {
                BufferedReader reader6 = new BufferedReader(new FileReader(file6));
                String line = "";
                while ((line = reader6.readLine()) != null)
                {
                    String[] data = line.split(",");

                    for (int i = 0; i < data.length; i++)
                    {
                        switch (i)
                        {
                            case 0:
                                goomba1Alive = Boolean.valueOf(data[i]);
                                break;
                            case 1:
                                goomba1X = Double.parseDouble(data[i]);
                                break;
                            case 2:
                                goomba1Y = Double.parseDouble(data[i]);
                                break;
                            case 3:
                                goomba1Row = Integer.parseInt(data[i]);
                                break;
                            case 4:
                                goomba1Column = Integer.parseInt(data[i]);
                                break;
                            case 5:
                                goomba1Identifier = Integer.parseInt(data[i]);
                                break;
                            case 6:
                                goomba2Alive = Boolean.valueOf(data[i]);
                                break;
                            case 7:
                                goomba2X = Double.parseDouble(data[i]);
                                break;
                            case 8:
                                goomba2Y = Double.parseDouble(data[i]);
                                break;
                            case 9:
                                goomba2Row = Integer.parseInt(data[i]);
                                break;
                            case 10:
                                goomba2Column = Integer.parseInt(data[i]);
                                break;
                            case 11:
                                goomba2Identifier = Integer.parseInt(data[i]);
                                break;
                            case 12:
                                goomba3Alive = Boolean.valueOf(data[i]);
                                break;
                            case 13:
                                goomba3X = Double.parseDouble(data[i]);
                                break;
                            case 14:
                                goomba3Y = Double.parseDouble(data[i]);
                                break;
                            case 15:
                                goomba3Row = Integer.parseInt(data[i]);
                                break;
                            case 16:
                                goomba3Column = Integer.parseInt(data[i]);
                                break;
                            case 17:
                                goomba3Identifier = Integer.parseInt(data[i]);
                                break;
                            case 18:
                                goombaCount = Integer.parseInt(data[i]);
                                break;
                        }
                    }
                }
                reader6.close();
            }
            catch (IOException e)
            {
                System.out.println(e.getMessage());
            }

            Player player = new Player("/main/resources/images/player.png", playerX, playerY, playerRow, playerColumn);
            player.setMaxPlaceableBombsCount(maxPlaceableBombsCount);
            player.setExplosionRadius(explosionRadius);

            Goomba goomba1 = null;
            Goomba goomba2 = null;
            Goomba goomba3 = null;

            if (goomba1Alive)
            {
                goomba1 = new Goomba("/main/resources/images/goomba.png", goomba1X, goomba1Y, goomba1Row, goomba1Column, goomba1Identifier, true);
            }

            if (goomba2Alive)
            {
                goomba2 = new Goomba("/main/resources/images/goomba.png", goomba2X, goomba2Y, goomba2Row, goomba2Column, goomba2Identifier, true);
            }

            if (goomba3Alive)
            {
                goomba3 = new Goomba("/main/resources/images/goomba.png", goomba3X, goomba3Y, goomba3Row, goomba3Column, goomba3Identifier, true);
            }

            Board board = new Board(level, player, goomba1, goomba2, goomba3);

            Stage s = (Stage) ((Node) event.getSource()).getScene().getWindow();

            GameTimerService timer = new GameTimerService();
            timer.setSeconds(seconds);
            timer.setMinutes(minutes);

            BoardController controller = new BoardController(board, new BoardModel(), s, timer);
            controller.setCurrentLevel(currentLevel);
            controller.setMaxBombUpgradeSpwan(bombUpgradeSpawnCount);
            controller.setMaxExplosionUpgradeSpawn(explosionUpgradeSpawnCount);
            controller.setGoombaCount(goombaCount);

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/main/resources/fxml/game_menu.fxml"));
            loader.setControllerFactory(param -> new GameMenuController(controller, timer));
            AnchorPane menu = loader.load();

            GameView game = new GameView(menu, board, controller, timer);

            stage.setBorderPane(s, game);

            game.requestFocus();
        }
        else
        {
            System.out.println("No game to resume");
        }
    }

    @FXML
    public void onClickHowToPlay(ActionEvent event) throws IOException
    {
        Stage s = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.changeFxml(s, "/main/resources/fxml/controls.fxml");
    }
}