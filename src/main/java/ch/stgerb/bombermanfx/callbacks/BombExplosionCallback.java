package main.java.ch.stgerb.bombermanfx.callbacks;

import main.java.ch.stgerb.bombermanfx.board.Block;
import main.java.ch.stgerb.bombermanfx.board.Bomb;
import main.java.ch.stgerb.bombermanfx.board.Explosion;

public interface BombExplosionCallback
{
    void onExplode(Bomb bomb);

    void afterExplosion(Explosion explosion, Block block);
}
