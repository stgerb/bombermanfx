package main.java.ch.stgerb.bombermanfx.callbacks;

import main.java.ch.stgerb.bombermanfx.board.Block;

public interface ItemCallback
{
    void onExplosionUpgradePickUp(Block block);

    void onBombUpgradePickUp(Block block);

    void onDoorCollision(Block block);
}